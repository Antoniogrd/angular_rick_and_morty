import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'markText'
})
export class MarkTextPipe implements PipeTransform {

  transform(value: unknown): unknown {
    // return null;
    return '*' + value + '*';
  }

}
