import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CharactersPageComponent } from './pages/characters-page/characters-page.component';
import { MenuComponent } from './core/components/menu/menu.component';
import { HttpClientModule } from '@angular/common/http';
import { GalleryComponent } from './shared/components/gallery/gallery.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FavouritesPageComponent } from './pages/favourites-page/favourites-page.component';
import { LocationsPageComponent } from './pages/locations-page/locations-page.component';
import { GalleryLocationsComponent } from './shared/components/gallery-locations/gallery-locations.component';
import { CharactersPageDetailComponent } from './pages/characters-page/characters-page-detail/characters-page-detail.component';
import { MarkTextPipe } from './shared/pipes/mark-text.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    CharactersPageComponent,
    MenuComponent,
    GalleryComponent,
    ContactPageComponent,
    FavouritesPageComponent,
    LocationsPageComponent,
    GalleryLocationsComponent,
    CharactersPageDetailComponent,
    MarkTextPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
