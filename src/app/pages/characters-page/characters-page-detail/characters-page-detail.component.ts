import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from 'src/app/shared/services/characters.service';

@Component({
  selector: 'app-characters-page-detail',
  templateUrl: './characters-page-detail.component.html',
  styleUrls: ['./characters-page-detail.component.scss']
})
export class CharactersPageDetailComponent implements OnInit {

  character: any = {};

  constructor(private route: ActivatedRoute, private characterService: CharactersService) { }

  ngOnInit(): void {
    // this.route.paramMap.subscribe(params => {
    //   const idCharacter = params.get('idCharacter');

    //   this.characterService.getCharacter(idCharacter).subscribe(res => {
    //     this.character = res;
    //   })
    // })
  }

}
