import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharactersPageDetailComponent } from './characters-page-detail.component';

describe('CharactersPageDetailComponent', () => {
  let component: CharactersPageDetailComponent;
  let fixture: ComponentFixture<CharactersPageDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharactersPageDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharactersPageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
