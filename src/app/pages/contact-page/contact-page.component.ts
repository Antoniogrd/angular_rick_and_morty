import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {
  contactForm;
  isSubmitted = false;

  constructor(private formBuilder: FormBuilder) 
  {
    this.contactForm = this.formBuilder.group({
      name: ['ej: Pepe', [Validators.required, Validators.minLength(3)]],
      surmame: ['ej: López', [Validators.required, Validators.maxLength(5), Validators.maxLength(64)]],
      age: ['ej: 23', [Validators.minLength(5), Validators.maxLength(99)]] ,
      email: ['ej: Pepe@gmail.com', [Validators.required, Validators.email]],
      description: [''],
    })
  }

//   Email -> de tipo email
// Apellidos - > minLength 5  maxLength 64
// Edad -> min 18 max 99

  ngOnInit(): void {
  }

  submitted(){
    this.isSubmitted = true;
    console.log(this.contactForm)
    console.log(this.contactForm.value)
  }

}
