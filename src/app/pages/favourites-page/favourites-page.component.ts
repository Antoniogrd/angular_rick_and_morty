import { FavoritesLocalService } from './../../shared/services/local/favorites-local.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favourites-page',
  templateUrl: './favourites-page.component.html',
  styleUrls: ['./favourites-page.component.scss']
})
export class FavouritesPageComponent implements OnInit {

  favoritesLocal;

  constructor(private favoritesLocalservice: FavoritesLocalService) { }

  ngOnInit(): void {
    this.favoritesLocal = this.favoritesLocalservice.getFavorites();
    console.log(this.favoritesLocal)
  }

}
